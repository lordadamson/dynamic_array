#include <chrono>
#include <iostream>
#include <vector>

struct Timer
{
	Timer() {}
	~Timer() {}

	void start_timer()
	{
		start = std::chrono::high_resolution_clock::now();
	}

	float get_elapsed_time()
	{
		using namespace std::chrono;

		std::chrono::high_resolution_clock::time_point end =
				high_resolution_clock::now();

		auto duration = duration_cast<microseconds>(end - start).count();
		float output = float(duration);
		return output;
	}

	std::chrono::high_resolution_clock::time_point start;
};

template <typename T>
struct darray
{
	T* data = nullptr;
	size_t size = 0;
	size_t capacity = 0;

	void extend();
	void push_back(T e);
	T& operator[](size_t i);
	T& at(size_t i);
	void reserve(size_t c);

	~darray();
};

template <typename T>
darray<T>::~darray()
{
	if(data)
	{
		free(data);
		data = nullptr;
	}
}

template <typename T>
T&
darray<T>::at(size_t i)
{
	return data[i];
}

template <typename T>
T&
darray<T>::operator[](size_t i)
{
	return at(i);
}

template <typename T>
void
darray<T>::reserve(size_t c)
{
	if(c == 0)
	{
		return;
	}

	if(c == capacity)
	{
		return;
	}

	T* new_data = static_cast<T*>(realloc(data, sizeof(T) *  c));

	if(new_data)
	{
		data = new_data;
		capacity = c;
		return;
	}

	new_data = static_cast<T*>(malloc(sizeof(T) * c));

	for(size_t i = 0; i < size; i++)
	{
		new_data[i] = data[i];
	}

	if(data)
	{
		free(data);
	}

	data = new_data;
	capacity = c;
}

template <typename T>
void
darray<T>::extend()
{
	if(capacity == 0)
	{
		reserve(1);
		return;
	}

	reserve(capacity * 2);
}

template <typename T>
void
darray<T>::push_back(T e)
{
	if(size < capacity)
	{
		data[size] = e;
		size++;
		return;
	}

	extend();
	data[size] = e;
	size++;
}

int main()
{
	size_t meh = 10000;
	using namespace std;
	Timer t;


	{
		darray<int> wow;

		t.start_timer();

		for(int i = 0; i < meh; i++)
		{
			wow.push_back(i);
		}

		cout << t.get_elapsed_time() << endl;
	}

	{
		vector<int> wow;

		t.start_timer();

		for(int i = 0; i < meh; i++)
		{
			wow.push_back(i);
		}

		cout << t.get_elapsed_time() << endl;
	}
}
