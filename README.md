# dynamic_array

A C++ implementation of a dynamic array that performs better than std::vector for some reason. It's incomplete and untested.

I think the reason it's faster than std::vector is that it uses `realloc()`?
I put a counter to see how many times realloc is used instead of `malloc()` and it seems that it is used 100% of the time.
Not sure if that will be the case in a bigger and more complicated application where the memory starts to fragment.

Also I benchmarked only with ints. Not sure if it would perform any differently with other types. In theory it should still be the same but I don't know.